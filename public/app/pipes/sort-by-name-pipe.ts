import {Pipe} from "angular2/angular2";

@Pipe({
  name: "sortByName",
  pure: false,
})
export class SortByNamePipe {
  public transform(array: Array<any>, args: string[]): Array<any> {
    if (typeof array !== "undefined") {
      return array.sort(function(a: any, b: any): number {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      });
    }
  }
}
