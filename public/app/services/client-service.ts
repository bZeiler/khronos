import { Injectable, Observable } from "angular2/angular2";
import { Http, Headers, HTTP_PROVIDERS, Response } from "angular2/http";

import { Client } from "../models/client";
import { Task } from "../models/task";
import { Worker } from "../models/worker";
import { Activity } from "../models/activity";

@Injectable()
export class ClientService {
  public http: Http;

  private url: string;
  private jsonHeaders: Object;

  constructor(http: Http) {
    this.http = http;
    this.url = "http://localhost:8080/api/";
    this.jsonHeaders = { headers: new Headers({
      "Accept": "application/json",
      "Content-Type": "application/json",
    }), };
  }

  // gets
  public getClients(): Observable<Response> {
    return this.http.get(this.url + "clients/");
  }

  // updates
  public setActivity(activity: Activity, aid: string,
                     wid: string, tid: string, cid: string): Observable<Response> {
    let url: string = this.url + "activities/" + cid + "/" + tid + "/" + wid + "/" + aid;
    return this.http.put(url, JSON.stringify(activity), this.jsonHeaders);
  }

  // creates
  public createClient(client: Client): Observable<Response> {
    let url: string = this.url + "clients/";
    return this.http.post(url, JSON.stringify(client), this.jsonHeaders);
  }
  public createTask(task: Task, cid: string): Observable<Response> {
      let url: string = this.url + "tasks/" + cid;
      return this.http.post(url, JSON.stringify(task), this.jsonHeaders);
  }
  public createWorker(worker: Worker, tid: string, cid: string): Observable<Response> {
    let url: string = this.url + "workers/" + cid + "/" + tid;
    return this.http.post(url, JSON.stringify(worker), this.jsonHeaders);
  }
  public createActivity(activity: Activity, wid: string,
                        tid: string, cid: string): Observable<Response> {
    let url: string = this.url + "activities/" + cid + "/" + tid + "/" + wid;
    return this.http.post(url, JSON.stringify(activity), this.jsonHeaders);
  }

  // deletes
  public deleteClient(cid: string): Observable<Response> {
    let url: string = this.url + "clients/" + cid;
    return this.http.delete(url, this.jsonHeaders);
  }
  public deleteTask(tid: string, cid: string): Observable<Response> {
    let url: string = this.url + "tasks/" + cid + "/" + tid;
    return this.http.delete(url, this.jsonHeaders);
  }
  public deleteWorker(wid: string, tid: string, cid: string): Observable<Response> {
    let url: string = this.url + "workers/" + cid + "/" + tid + "/" + wid;
    return this.http.delete(url, this.jsonHeaders);
  }
}
