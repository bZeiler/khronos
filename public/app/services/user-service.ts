import { Injectable, Observable } from "angular2/angular2";
import { Http, Headers, HTTP_PROVIDERS, Response } from "angular2/http";

import { User } from "../models/user";

@Injectable()
export class UserService {
  public http: Http;

  private url: string;
  private jsonHeaders: Object;

  constructor(http: Http) {
    this.http = http;
    this.url = "http://localhost:8080/api/users/";
    this.jsonHeaders = {
      "Accept": "application/json",
      "Content-Type": "application/json",
    };
  }

  public getUser(uid: string): Observable<Response> {
    return this.http.get(this.url + uid);
  }
}
