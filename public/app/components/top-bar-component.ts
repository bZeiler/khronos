import {Component, CORE_DIRECTIVES} from "angular2/angular2";

@Component({
  selector: "top-bar",
  templateUrl: "app/views/top-bar.component.html",
  directives: [CORE_DIRECTIVES],
})
export class TopBarComponent {}
