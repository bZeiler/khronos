import { Component, CORE_DIRECTIVES, FORM_DIRECTIVES, Observable } from "angular2/angular2";
import { Http, Headers, HTTP_PROVIDERS, Response } from "angular2/http";

import { ClientService } from "../services/client-service";
import { UserService } from "../services/user-service";
import { Utils } from "../utils/util";
import { SortByNamePipe } from "../pipes/sort-by-name-pipe";

import { Client } from "../models/client";
import { Task } from "../models/task";
import { Worker } from "../models/worker";
import { Activity } from "../models/activity";
import { User } from "../models/user";

@Component({
  selector: "client-wrapper",
  providers: [ClientService, UserService, Utils, HTTP_PROVIDERS],
  templateUrl: "app/views/client.component.html",
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
  pipes: [SortByNamePipe],
})
export class ClientComponent {
  public me: User;

  private weeksDisplayed: number;
  private cservice: ClientService;
  private uservice: UserService;
  private utils: Utils;
  private clients: Array<Client>;

  constructor(cservice: ClientService, utils: Utils, uservice: UserService) {
    this.utils = utils;
    this.cservice = cservice;
    this.uservice = uservice;
    this.weeksDisplayed = 3;
    this.uservice.getUser("5658c4de30f90e4b298d8bae")
      .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
      .subscribe(
        (data: any) => this.me = data,
        (err: any) => { console.log("getUser():", err); },
        () => console.log("Got user")
      );

    this.cservice.getClients()
      .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
      .subscribe(
        (data: any) => this.setResults(data),
        (err: any) => { console.log("getClients():", err); },
        () => console.log("Got clients")
      );
  }

  private setResults(clients: Array<Client>): void {
    this.utils.fillEmptyDays(clients, this.weeksDisplayed)
      .then((result: Array<Client>) => {
      this.clients = result;
    });
  }

  // creation methods
  private createClient(): void {
    let randClientName: number = Math.floor((Math.random() * 100) + 1);
    let client: Client = new Client("Client " + randClientName, false, []);
    this.cservice.createClient(client)
      .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
      .subscribe(
        (data: any) => this.clients.push(data),
        (err: any) => { console.log("createClient():", err); },
        () => console.log("Created client")
      );
  }

  private createTask(client: Client): void {
    let randTaskName: number = Math.floor((Math.random() * 100) + 1);
    let task: Task = new Task("Task " + randTaskName, this.me, false, []);
    this.cservice.createTask(task, client._id)
      .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
      .subscribe(
        (data: any) => client.tasks.push(data),
        (err: any) => { console.log("createTask():", err); },
        () => console.log("Created task")
      );
  }

  private createWorker(client: Client, task: Task): void {
    let worker: Worker = new Worker(this.me, []);
    this.cservice.createWorker(worker, task._id, client._id)
      .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
      .subscribe(
        (data: any) => task.workers.push(data),
        (err: any) => { console.log("createWorker():", err); },
        () => console.log("Created worker")
      );
  }

  // delete methods
  private deleteClient(client: Client, btn: HTMLButtonElement): void {
    btn.disabled = true;
    let clientPos: number = this.getIndexByObjectId(this.clients, client._id);
    this.cservice.deleteClient(client._id)
      .map((res: any) => this.utils.statusOK(res) ? res.text() : "")
      .subscribe(
        (data: any) => this.clients.splice(clientPos, 1),
        (err: any) => {
          console.log("deleteClient():", err);
          btn.disabled = false;
        },
        () => {
          console.log("Deleted client");
          btn.disabled = false;
        }
      );
  }

  private deleteTask(task: Task, client: Client, btn: HTMLButtonElement): void {
    btn.disabled = true;
    let taskPos: number = this.getIndexByObjectId(client.tasks, task._id);
    this.cservice.deleteTask(task._id, client._id)
      .map((res: any) => this.utils.statusOK(res) ? res.text() : "")
      .subscribe(
        (data: any) => client.tasks.splice(taskPos, 1),
        (err: any) => {
          console.log("deleteTask():", err);
          btn.disabled = false;
        },
        () => {
          console.log("Deleted task!");
          btn.disabled = false;
        }
      );
  }

  private deleteWorker(worker: Worker, task: Task, client: Client, btn: HTMLButtonElement): void {
    btn.disabled = true;
    let workerPos: number = this.getIndexByObjectId(task.workers, worker._id);
    this.cservice.deleteWorker(worker._id, task._id, client._id)
      .map((res: any) => this.utils.statusOK(res) ? res.text() : "")
      .subscribe(
        (data: any) => task.workers.splice(workerPos, 1),
        (err: any) => {
          console.log("deleteWorker():", err);
          btn.disabled = false;
        },
        () => {
          console.log("Deleted worker!");
          btn.disabled = false;
        }
      );
  }

  // activities
  private getTotalTime(worker: Worker): number {
    let total: number = 0;
    if (worker != null) {
      // wanted to make this into a for(let ) function, but that doesn't
      // work for some obscure reason.. Keeping old forEach syntax...
      worker.activities.forEach(function(element: Activity): void {
        if (!isNaN(element.hours)) {
          total += element.hours;
        }
      });
    }

    return total;
  }

  private handleActivityFocus(activity: Activity): void {
    activity.prevHours = activity.hours;
    // send socket io -- updating
  }

  private handleActivityBlur(activity: Activity, aid: string,
                             wid: string, tid: string,
                             cid: string): void {
    /*
      Sooo. It turns out that mongodb is very bad at dealing with
      nested structures and since we sort of rely on that here
      we are a little screwed. I've found a method for it using chained
      id's so that means we need to send all the id's of the parents
      with us when we update an activity *sigh*. So here goes.
    */

    if (isNaN(activity.hours)) {
      console.log("Error: NaN value as hours - resetting field");
      // doing some weird variable assigning here, but I couldn't
      // get it to work without it. ng-model is acting weird...
      // maybe a problem with angular alpha?

      let testVar: number = activity.prevHours;
      setTimeout((): void => { activity.hours = testVar; }, 100);
    } else {
      if (activity.hours !== activity.prevHours) {
        if (typeof aid !== "undefined") {
          console.log("Updating activity", activity);
          this.cservice.setActivity(activity, aid, wid, tid, cid)
            .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
            .subscribe(
              (data: any) => console.log(data),
              (err: any) => { console.log("setActivity():", err); },
              () => console.log("Activity updated")
            );
        } else {
          activity.creator = this.me;
          console.log("Creating new activity", activity);
          this.cservice.createActivity(activity, wid, tid, cid)
            .map((res: any) => this.utils.statusOK(res) ? res.json() : "")
            .subscribe(
              (data: any) => activity._id = data._id,
              (err: any) => { console.log("createActivity():", err); },
              () => console.log("Activity created")
            );
        }
      } else {
        console.log("Not updating activity - hours are the same");
      }
    }
    delete activity.prevHours;
    // send socket.io - updated
  }

  private getIndexByObjectId(array: Array<any>, id: string): number {
    let res: number = array.map((o: any): string => {
      return o._id;
    }).indexOf(id);
    return res;
  }
}
