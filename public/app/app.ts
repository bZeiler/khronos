import {bootstrap, Component} from "angular2/angular2";

import { TopBarComponent } from "./components/top-bar-component";
import { ClientComponent } from "./components/client-component";
import { User } from "./models/user";

@Component({
  selector: "khronos-app",
  templateUrl: "app/views/khronos-app.component.html",
  directives: [TopBarComponent, ClientComponent],
})
export class KhronosApp {
  public user: User = new User("bzh", "Bjørn", 0);
}

bootstrap(KhronosApp);
