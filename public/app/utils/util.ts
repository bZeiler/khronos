/// <reference path="../../typings/tsd.d.ts" />

import { Injectable } from "angular2/angular2";
import { Client } from "../models/client";
import { Activity } from "../models/activity";
import * as moment from "moment";

@Injectable()
export class Utils {
  public fillEmptyDays(clients: Array<Client>, weeks: number): Promise<Client[]> {
    let promise: Promise<Client[]> = new Promise(function(resolve: any, reject: any): void {
      let startMoment: Moment = moment().startOf("week").add(1, "days");
      let endMoment: Moment = moment().add(weeks - 1, "weeks");
      let days: number = endMoment.diff(startMoment, "days");

      for (let client of clients) {
        for (let task of client.tasks) {
          for (let worker of task.workers) {
            let daysArray: Array<Activity> = [];
            let thisDay: Moment = moment(startMoment);
            for (let activity of worker.activities) {
              let activityDay: Moment = moment(activity.date);
              let arrayIndex: string = activityDay.diff(startMoment, "days");
              daysArray[arrayIndex] = activity;
            }

            for (let i: number = 0; i <= days; i++) {
              let isToday: boolean = thisDay.isSame(moment(), "day");
              let dayOfWeek: Object = thisDay.day();
              if (dayOfWeek === 6 || dayOfWeek === 0) {
                thisDay.add(1, "days");
                continue;
              }

              if (typeof daysArray[i] === "undefined") {
                daysArray[i] = new Activity(0, moment(startMoment).add(i, "days").format());
              }
              thisDay.add(1, "days");
            }
            worker.activities = daysArray;
          }
        }
      }

      resolve(clients);
    });

    return promise;
  }

  public statusOK(res: any): boolean {
    if (res.status < 200 || res.status >= 300) {
      throw new Error(res.text());
    }
    return true;
  }
}
