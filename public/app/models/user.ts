export class User {
  public _id: string;
  public nick: string;
  public name: string;
  public privLevel: number;

  constructor(nick: string, name: string, privLevel: number) {
    this.nick = nick;
    this.name = name;
    this.privLevel = privLevel;
  }
}
