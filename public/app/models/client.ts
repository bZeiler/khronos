import { Task } from "./task";

export class Client {
  public _id: string;
  public name: string;
  public archived: boolean;
  public tasks: Array<Task>;

  constructor(name: string, archived: boolean, tasks: Array<Task>) {
    this.name = name;
    this.archived = archived;
    this.tasks = tasks;
  }
}
