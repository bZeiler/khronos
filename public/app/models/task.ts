import { Worker } from "./worker";
import { User } from "./user";

export class Task {
  public _id: string;
  public name: string;
  public creator: User;
  public archived: boolean;
  public workers: Array<Worker>;

  constructor(name: string, creator: User, archived: boolean, workers: Array<Worker>) {
    this.name = name;
    this.creator = creator;
    this.archived = archived;
    this.workers = workers;
  }
}
