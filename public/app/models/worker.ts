import { Activity } from "./activity";
import { User } from "./user";

export class Worker {
  public _id: string;
  public user: User;
  public activities: Array<Activity>;

  constructor(user: User, activities: Array<Activity>) {
    this.user = user;
    this.activities = activities;
  }
}
