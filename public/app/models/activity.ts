import { User } from "./user";

export class Activity {
  public _id: string;
  public hours: number;
  public date: string;
  public creator: User;
  public prevHours: number;

  constructor(hours: number, date: string,
              creator: User) {
    this.hours = hours;
    this.date = date;
    this.creator = creator;
  }
}
