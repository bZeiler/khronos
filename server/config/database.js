module.exports = {
  dbUrl  : "mongodb://localhost:27017/khronosdb",
  apiUrl : "http://localhost:8080/api",
  port   : "8080"
};