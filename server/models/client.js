var mongoose = require('mongoose');

var Activity = new mongoose.Schema({
  hours     :   Number,
  date      :   { type: Date, default: Date.now },
  creator   :   { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

var Worker = new mongoose.Schema({
  user      :   { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  activities:   [ Activity ]
});

var Task = new mongoose.Schema({
  name      :   String,
  creator   :   { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  archived  :   { type: Boolean, default: false },
  workers   :   [ Worker ]
});

var ClientSchema = new mongoose.Schema({
  name      :   String,
  archived  :   { type: Boolean, default: false },
  tasks     :   [ Task ]
});

//Models
module.exports = mongoose.model( 'Client', ClientSchema );