var mongoose = require('mongoose'),
    bcrypt   = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

//Schemas
var UserSchema = mongoose.Schema({
    name: String,
    nick: String,
    privLevel: Number
});

UserSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

//Models
module.exports = mongoose.model( 'User', UserSchema );