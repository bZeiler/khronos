var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var Client    = require('../models/client');
var Utils     = require('../utils');

router.post( '/:cid/:tid', function( req, res ) {
  var p = req.params;

  return Client.findById( p.cid, function( err, client ) {

    var t = client.tasks.id(p.tid);

    t.workers.push(req.body);

    client.save( function( err, savedObj ) {
      if( !err ) {
        console.log( 'Created successfully. Finding...' );

        Client
        .findById( p.cid )
        .populate( "tasks.workers.user" )
        .exec( function( err, client) {
          if( !err ) {
            var resultingObj = client.tasks.id(p.tid).workers.slice(-1).pop();
            if( resultingObj !== null ) {
              console.log( 'Object refound in db ', resultingObj );
              return res.status(200).send( resultingObj );
            }
          }

          console.log( 'ERROR: Could not find object after creation', err );
          return res.status(400).send( 'ERROR: Creation failed' );
        });
      } else {
        console.log( 'Creation failed', err )
        return res.status(400).send( 'ERROR: Creation failed' );
      }
    });
  })
});

router.delete( '/:cid/:tid/:wid', function( req, res ) {
  var p = req.params;

  console.log( 'Deleting worker: ' + p.wid + ' from task: ' + p.tid );
  return Client.findById( p.cid, function( err, client ) {
    if( !err ) {
      if( client === null ) {
        return Utils.err("Client not found", p.cid, res);
      }

      var worker = client.tasks.id( p.tid ).workers.id( p.wid );
      if( worker === null ) {
        return Utils.err("Worker not found", p.wid, res);
      }

      worker.remove();

      return client.save( function( err ) {
        if( !err ) {
          return Utils.succ('Client with deleted worker saved successfully', res);
        } else {
          return Utils.err("worker not deleted", p.wid, res);
        }
      });  
    } else {
      return Utils.err("Error finding workers client by id", p.cid, res);
    }
  });
});

module.exports = router;