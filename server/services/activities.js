var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var Client    = require('../models/client');

router.put( '/:cid/:tid/:wid/:aid', function( req, res ) {
  return Client.findById( req.params.cid, function( err, client ) {
    var t = client.tasks.id(req.params.tid)
      .workers.id(req.params.wid)
      .activities.id(req.params.aid);

    t.hours = req.body.hours;
    t.creator = req.body.creator;
    
    client.save( function( err ) {
      if( !err ) {
        console.log( 'Updated successfully' );
        return res.send(client);
      } else {
        return res.send( console.log( 'Update failed', err ) );
      }
    });
  });
});

router.post( '/:cid/:tid/:wid', function( req, res ) {
  var p = req.params;

  return Client.findById( p.cid, function( err, client ) {
    var t = client.tasks.id(p.tid).workers.id(p.wid);

    t.activities.push(req.body);

    client.save( function( err, savedObj ) {
      if( !err ) {
        var resultingObj = t.activities.slice(-1).pop();
        console.log( 'Created successfully ', resultingObj );
        return res.send( resultingObj );
      } else {
        console.log( 'Creation failed', err )
        return res.send( 'ERROR: Creation failed' );
      }
    });
  })
});


module.exports = router;