var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var Client    = require('../models/client');

router.get( '/', function( req, res ) {
  var client = new Client;

  return Client.find({})
    .populate('tasks.creator')
    .populate('tasks.workers.user')
    .populate('tasks.workers.activities.creator')
    .exec( function( err, clients ) {
      if( !err ) {
        return res.send( clients );
      } else {
        return console.log( err );
      }
  });
});

router.post( '/', function( req, res ) {
  console.log( "Creating client: " + req.body.name );
  var client = new Client( req.body );
  client.save( function( err ) {
    if( !err ) {
      console.log( 'Created client successfully...');
      return res.send( client );
    } else {
      return res.send( console.log( err ) );
    }
  });
});

router.delete( '/:id', function( req, res ) {
  console.log( 'Deleting client with id: ', req.params.id );
  return Client.findById( req.params.id, function( err, client ) {
    if( !err ) {
      if( client === null ) return res.send( console.log( "ERROR: Client not found with id: " + req.params.id ) ); 

      return client.remove( function( err ) {
        if( !err ) {
          console.log( 'Client removed successfully...' )
          return res.send( 'Client deleted successfully' );
        } else {
          console.log( err )
          return res.send( 'Error deleting client' );
        }
      });  
    } else {
      console.log( err )
      return res.send( 'Error finding client by id' );
    }
  });
});

module.exports = router;