var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var User      = require('../models/user');

router.get( '/:id', function( req, res ) {
  return User.findById( req.params.id, function( err, user ) {
    if( !err ) {
      console.log( 'Found user successfully' );
      return res.send( user );
    } else {
      console.log( 'Error finding user' );
      return res.send( 'Error finding user' );
    }
  });
});

module.exports = router;