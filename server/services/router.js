var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var User = require('../models/user');
var Conf = require('../config/database');
var async = require('async');

// Show home view
router.get( '/', function( req, res ) {
  // res.render('index.html', { 
  //   user : req.user,
  //   conf : Conf
  // });
  res.render('index.html');
});

// router.get( '/profile', function( req, res ) {
//   res.render( 'profile.ejs', { user : req.user } );
// });

// router.get( '/users', function( req, res ) {
//   if( req.user.privLevel > 0 ) res.redirect('/');

//   User.find( function( err, usersRes ) {
//     if( !err ) {
//       res.render('users.ejs', {
//         users : usersRes,
//         conf : Conf
//       });  
//     } else {
//       res.send( console.log( err ) );
//     }
//   });
// });

// router.get('/login', function( req, res ) {
//   res.render('login.ejs', { message: req.flash('loginMessage') });
// });

// // process the login form
// router.post('/login', passport.authenticate('local-login', {
//   successRedirect : '/', // redirect to the secure profile section
//   failureRedirect : '/login', // redirect back to the signup page if there is an error
//   failureFlash : true // allow flash messages
// }));

// router.get('/logout', function(req, res) {
//   req.logout();
//   res.redirect('/login');
// });

// router.get('/signup', function( req, res ) {
//   res.render('signup.ejs', { message: req.flash('signupMessage') });
// });

// // process the signup form
// router.post('/signup', passport.authenticate('local-signup', {
//   successRedirect : '/', // redirect to the secure profile section
//   failureRedirect : '/signup', // redirect back to the signup page if there is an error
//   failureFlash : true // allow flash messages
// }));

// router.get('/forgotpassword', function( req, res ) {
//   res.render('forgotpassword.ejs', { message: req.flash('forgotMessage') });
// });

// router.post('/forgotpassword', function( req, res ) {
//   User.findOne( { nick: req.body.nick }, function( err, user ) {
//     if ( !user ) {
//       req.flash( 'forgotMessage', 'No account with that username exists.');
//       return res.redirect('/forgotpassword');
//     }

//     user.password = user.generateHash( "1234" );

//     user.save(function(err) {
//       if( !err ) {
//         req.flash('forgotMessage', 'user password has been reset');
//         return res.redirect('/forgotpassword');
//       } else {
//         console.log( "error in user password reset" );
//       }
//     });
//   });
// });

module.exports = router;