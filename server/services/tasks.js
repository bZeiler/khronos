var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var Client    = require('../models/client');
var Utils     = require('../utils');

router.post( '/:cid', function( req, res ) {
  var p = req.params;
  return Client.findById( p.cid, function( err, client ) {
    client.tasks.push(req.body);
    client.save( function( err, savedObj ) {
      if( !err ) {
        var resultingObj = client.tasks.slice(-1).pop();
        console.log( 'Created successfully ', resultingObj );
        return res.send( resultingObj );
      } else {
        return Utils.err("Saving client with new task failed", req.params.cid, res);
      }
    })
  })
});

router.delete( '/:cid/:tid', function( req, res ) {
  console.log( 'Deleting task: ' + req.params.tid + ' from client: ' + req.params.cid );
  return Client.findById( req.params.cid, function( err, client ) {
    if( !err ) {
      if( client === null ) {
        return Utils.err("Client not found", req.params.cid, res);
      }

      var task = client.tasks.id( req.params.tid );
      if( task === null ) {
        return Utils.err("Task not found", req.params.cid, res);
      }

      task.remove();

      return client.save( function( err ) {
        if( !err ) {
          return Utils.succ('Client with deleted task saved successfully', res);
        } else {
          return Utils.err("Task not deleted", req.params.cid, res);
        }
      });  
    } else {
      return Utils.err("Error finding tasks client by id", req.params.cid, res);
    }
  });
});

module.exports = router;