var Utils = {
  err: function(msg, obj, res) {
    console.log("ERROR: " + msg + ": " + obj );
    res.status(400).send(msg + ": " + obj);
  },
  succ: function(msg, res) {
    console.log("SUCCESS: " + msg);
    res.status(200).send( "SUCCESS: " + msg);
  }
};

module.exports = Utils;
