var express         = require('express'),
    flash           = require('connect-flash'),
    path            = require('path'),
    mongoose        = require('mongoose'),
    passport        = require('passport'),
    session         = require('express-session'),
    mongostore      = require('connect-mongo')(session),
    compress        = require('compression'),
    morgan          = require('morgan'),
    methodOverride  = require('method-override'),
    bodyParser      = require('body-parser'),
    errorHandler    = require('errorhandler'),
    cookieParser    = require('cookie-parser');

var passportConfig  = require('./config/passport')(passport),
    databaseConfig  = require('./config/database');

var app = express();

var http  = require('http').Server(app);
var io    = require('socket.io')(http);

mongoose.connect( databaseConfig.dbUrl );

app.set('port', process.env.PORT || databaseConfig.port );

app.use(express.static(path.join(__dirname, '../public')));
app.use('/scripts/', express.static(path.join(__dirname, '../node_modules/')));

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); // set up ejs for templating
app.engine('html', require('ejs').renderFile);

app.use(session({
  secret: 'khronosthegodoftime',
  store: new mongostore({
    url : databaseConfig.dbUrl
  }),
  saveUninitialized: true,
  resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

/* CHECK LOGIN */
// var user = "";
// app.use(function (req, res, next) {
//   if( req.path == '/login' || req.path == '/signup' || req.path == '/forgotpassword' ) return next();
//   if( req.isAuthenticated() ) return next();
//   res.redirect('/login');
// });

/* API routes */
app.use('/api/clients', require('./services/clients'));
app.use('/api/tasks', require('./services/tasks'));
app.use('/api/workers', require('./services/workers'));
app.use('/api/users', require('./services/users'));
app.use('/api/activities', require('./services/activities'));

/* Actual route */
app.use('/', require('./services/router'));

/* SOCKET STUFF */
// var users = {};

// io.on( 'connection', function( socket ) {
//   console.log('user connected: ', socket.id);
//   users[socket.id] = "waiting..";

//   socket.on('disconnect', function(){
//     console.log('user left: ', socket.id);
//     delete users[socket.id];
//     io.emit('user left', users);
//   });

//   socket.on( 'user sending id', function( socketsNick ) {
//     users[socket.id] = socketsNick;
//     console.log('users connected: ', users);
//     io.emit('user connected', users);
//   });

//   socket.on( 'somethingWasChanged', function( whoFired ) {
//     console.log("CHANGE: Reloading everyone's page but: " + whoFired );
//     io.emit( 'somethingWasChanged', whoFired );
//   });
// });

if('development' == app.get('env')) {
  app.use(errorHandler());
}

http.listen(app.get('port'), function(){
  console.log('Magic happens on port ' + app.get('port'));
});
