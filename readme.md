#Khronos
Pet-project in order to proper familiarize myself with Angular 2, the new JS functions (Promises, arrow notation, let/var, etc.), TypeScript, relational structure in MongoDB, and possibly Falcor in the future.  

Rewriting a resource management tool that I originally built in backbone.js (also on a mongo/express/node stack) for Kunde & Co., because the code got too hairy, was too old, had too many hacks, and just generally had ended up like what I assume an angry ex-wife would be like.

###To-do
####Backend:
Rewrite semi-copypasted code to TypeScript, rewrite implied functions to arrow notation, introduce classes, make sure of proper seperation of concerns and get rid of the god-awful "var" notation.  
####Frontend:
So much here... Important part - which will probably be pushed till late in the phase - better promise handling and change from window.fetch to angular HTTP object.

###Technologies
Fundamentally a futuristic MEAN stack:  

(M)ongoDB  
(E)xpress  
(A)ngular 2.0 (Alpha build v44)  
(N)ode.js

####Additionally:
SystemJS (powerful module loader, ES6, AMD, the whole shebang)  
Mongoose  
Passport  
TypeScript (using the tsc compiler)  
ES6/7

###Build tools
Build tools are npm, no gulp at this time as no work has gone into frontend stuff yet (no css, etc.).

####Commands
npm run tsc  
npm start